import React, {Component} from 'react';
import {Router, Switch, Route} from 'react-router-dom';
import history from '../history';

import Dashboard from '../pages/Dashboard';

export default class RouterPages extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route path={"/"} exact render={() => {
                        return <Dashboard />
                    }} />
                    <Route path={"/Dashboard"} exact render={() => {
                        return <Dashboard />
                    }} />
                    <Route path={"/*"} render={() => {
                        return (
                            <div>
                                <h1>Page Not Found!</h1>
                            </div>
                        )
                    }} />
                </Switch>
            </Router>
        )
    }
}