import React, { Component } from 'react';
import { observable, toJS, action } from 'mobx';
import axios from 'axios';
import { observer } from 'mobx-react';

const appState = observable({
    productList: [],
    page: 1,
    isLoading: false,
    isPaging: false,
    cors: 'https://cors-anywhere.herokuapp.com/',
    header: {
        headers: {
            'openapikey': '721407f393e84a28593374cc2b347a98'
        }
    },
    clickProduct: action(() => {
        appState.getProduct(appState.page)
    }),
    clickNext: action(() => {
        if (appState.productList.length != 0) {
            appState.getProduct(appState.page+=1)
        }
    }),
    clickPrev: action(() => {
        if (appState.page != 1) {
            appState.getProduct(appState.page-=1)
        }
    })
});

appState.xmlToJson = function(xml) {
    let dataProduct = []
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(xml,"text/xml").getElementsByTagName('Products')[0].childNodes;
    if (xmlDoc) {
        for (var i = 0; i < xmlDoc.length; i++) {
            dataProduct[i] = {
                'product_name': xmlDoc[i].getElementsByTagName("prdNm")[0].childNodes[0].nodeValue,
                'sku': xmlDoc[i].getElementsByTagName("sellerPrdCd")[0] ? xmlDoc[i].getElementsByTagName("sellerPrdCd")[0].childNodes[0].nodeValue : '-',
                // 'image': xmlDoc[i].getElementsByTagName("sellerPrdCd")[0].childNodes[0].nodeValue,
                // 'description': xmlDoc[i].getElementsByTagName("sellerPrdCd")[0].childNodes[0].nodeValue,
                'price': xmlDoc[i].getElementsByTagName("selPrc")[0].childNodes[0].nodeValue
            }
            
        }
        appState.productList = dataProduct
    }
};

appState.getProduct = function(page) {
    appState.isLoading = true
    axios.get(appState.cors + 'http://api.elevenia.co.id/rest/prodservices/product/listing?page=' + page, appState.header)
    .then((res) => {
        if (res.data) {
            appState.xmlToJson(res.data)
            appState.isLoading = false
            appState.isPaging = true
        }
    })
    .catch((err) => {
        console.log(err)
        appState.isLoading = false
    })
};

appState.saveProduct = function(req, res) {
    appState.isLoading = true
    axios.get('http://localhost:5000/api/hello')
    .then((res) => {
        
    })
}

const Products = observer(props => {
    let dataProduct = []
    let product = toJS(props.appState.productList)
    product.forEach((element, index) => {
        dataProduct[index] = <div key={index} 
                                  className="card border-secondary mb-3 col-md-4" 
                                  style={{maxWidth: "18rem", minHeight: "18rem"}}>
                                <div className="card-header">{element.product_name}</div>
                                <div className="card-body text-secondary">
                                    <h5 className="card-title">{element.sku}</h5>
                                    <p className="card-text">{element.price}</p>
                                </div>
                            </div>
    });
    if (props.appState.isLoading) {
        return <label>Loading....</label>
    } else {
        if (props.appState.productList.length == 0 && props.appState.isPaging) {
            return <h4 className="text-danger">Empty!</h4>
        } else {
            return dataProduct
        }
    }
});

const Paging = observer(props => {
    if (props.appState.isPaging) {
        return <span>
            <button className="btn btn-default" style={{marginLeft: "10px"}} onClick={appState.clickPrev}>Prev Page</button>
            <button className="btn btn-default" style={{marginLeft: "10px"}} onClick={appState.clickNext}>Next Page</button>
            {
                props.appState.productList.length != 0 ? 
                <button style={{marginLeft: "10px"}} className="btn btn-success" onClick={appState.saveProduct}>SAVE</button> : ''
            }
        </span>
    }
});

export default class Dashboard extends Component {
    render() {
        return (
            <div>
                <div className="container">
                    <h1>Dashboard</h1>
                    <div>
                        <button className="btn btn-primary" onClick={appState.clickProduct}>Get Product</button>
                        <Paging appState={appState} />
                    </div>
                    <div>
                        <Products appState={appState} />
                    </div>
                </div>
            </div>
        )
    }
};