import React, {Component} from 'react';

import RouterPages from './components/RouterPages';

import './App.css';
import './assets/css/index.css'

export default class App extends Component {
  render() {
    return (
      <RouterPages />
    )
  }
}